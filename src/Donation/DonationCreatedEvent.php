<?php

namespace Adranetwork\CrmServiceEvents\Donation;

use Adranetwork\CrmServiceEvents\Traits\EventHelper;

class DonationCreatedEvent extends DonationEvent
{
    use EventHelper;

    public function getEventName(): string
    {
        return 'donation.created';
    }
}
