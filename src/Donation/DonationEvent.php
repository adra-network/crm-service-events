<?php

namespace Adranetwork\CrmServiceEvents\Donation;

use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\AdraCloud\Enums\PaymentStatus;
use Adranetwork\AdraCloud\EventSource\Event;
use Adranetwork\AdraCloud\EventSource\StreamEvent;
use App\Enums\DonationSource;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use PrinsFrank\Standards\Currency\ISO4217_Alpha_3;


abstract class DonationEvent extends StreamEvent implements Event
{

    public function __construct(
        public ?string $organizationId,
        public readonly string $id,
        public readonly string $donorId,
        public readonly int $amount,
        public readonly ISO4217_Alpha_3 $currency,
        public readonly PaymentProvider $paymentProviderName,
        public readonly ?string $paymentProviderReferenceId,
        public readonly PaymentStatus $status,
        public readonly DonationSource $source,
        public readonly Arrayable $info,
        public readonly Carbon $donatedAt,
        public readonly Carbon $createdAt,
        public readonly Carbon $updatedAt,

    )
    {
        parent::__construct();
        $this->organizationId = $this->organizationId ?? tenant('id');
    }

    public function jsonSerialize(): mixed
    {
        return [
            'organizationId' => $this->organizationId,
            'id' => $this->id,
            'donorId' => $this->donorId,
            'amount' => $this->amount,
            'currency' => $this->currency,
            'paymentProviderName' => $this->paymentProviderName,
            'paymentProviderReferenceId' => $this->paymentProviderReferenceId,
            'status' => $this->status->value,
            'source' => $this->source->value,
            'info' => $this->info->toArray(),
            'donatedAt' => $this->donatedAt->toIso8601ZuluString(),
            'createdAt' => $this->createdAt->toIso8601ZuluString(),
            'updatedAt' => $this->updatedAt->toIso8601ZuluString()
        ];
    }
}
