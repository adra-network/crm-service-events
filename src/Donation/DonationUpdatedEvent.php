<?php

namespace Adranetwork\CrmServiceEvents\Donation;

use Adranetwork\CrmServiceEvents\Traits\EventHelper;

class DonationUpdatedEvent extends DonationEvent
{
    use EventHelper;

    public function getEventName(): string
    {
        return 'donation.updated';
    }
}
