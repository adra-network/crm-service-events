<?php

namespace Adranetwork\CrmServiceEvents\Donor;

use Adranetwork\AdraCloud\EventSource\Event;
use Adranetwork\AdraCloud\EventSource\StreamEvent;
use App\Models\Donation;
use App\Models\Donor;


abstract class DonorEvent extends StreamEvent implements Event
{

    public function __construct(
        public ?string $organizationId,
        public readonly string $id,
        public readonly ?string $firstName,
        public readonly ?string $lastName,
        public readonly ?string $gender,
        public readonly ?string $birthday,
        public readonly ?string $title,
        public readonly ?string $salutation,
        public readonly array $alternateNames,
        public readonly string $createdAt,
        public readonly string $updatedAt,

    )
    {
        parent::__construct();
        $this->organizationId = $this->organizationId ?? tenant('id');
    }

    public function jsonSerialize(): mixed
    {
        return [
            'organizationId' => $this->organizationId,
            'id' => $this->id,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'gender' => $this->gender,
            'birthday' => $this->birthday,
            'title' => $this->title,
            'salutation' => $this->salutation,
            'alternateNames' => $this->alternateNames,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
        ];
    }
}
