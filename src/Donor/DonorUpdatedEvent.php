<?php

namespace Adranetwork\CrmServiceEvents\Donor;

use Adranetwork\CrmServiceEvents\Traits\EventHelper;

class DonorUpdatedEvent extends DonorEvent
{
    use EventHelper;

    public function getEventName(): string
    {
        return 'donor.updated';
    }
}
