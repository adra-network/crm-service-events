<?php

namespace Adranetwork\CrmServiceEvents\Donor;

use Adranetwork\CrmServiceEvents\Traits\EventHelper;

class DonorDeletedEvent extends DonorEvent
{
    use EventHelper;

    public function getEventName(): string
    {
        return 'donor.deleted';
    }
}
