<?php

namespace Adranetwork\CrmServiceEvents\DonorEmail;

use Adranetwork\AdraCloud\EventSource\Event;
use Adranetwork\AdraCloud\EventSource\StreamEvent;


abstract class DonorEmailEvent extends StreamEvent implements Event
{

    public function __construct(
        public ?string $organizationId,
        public readonly string $id,
        public readonly string $donorId,
        public readonly ?string $type,
        public readonly ?string $status,
        public readonly string $address,
        public readonly bool $default,
        public readonly string $createdAt,
        public readonly string $updatedAt,

    )
    {
        parent::__construct();
        $this->organizationId = $this->organizationId ?? tenant('id');

    }

    public function jsonSerialize(): mixed
    {
        return [
            'organizationId' => $this->organizationId,
            'id' => $this->id,
            'donorId' => $this->donorId,
            'type' => $this->type,
            'status' => $this->status,
            'address' => $this->address,
            '$this->default' => $this->default,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
        ];
    }
}
