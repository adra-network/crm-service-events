<?php

namespace Adranetwork\CrmServiceEvents\DonorEmail;


use Adranetwork\CrmServiceEvents\Traits\EventHelper;

class DonorEmailUpdatedEvent extends DonorEmailEvent
{
    use EventHelper;

    public function getEventName(): string
    {
        return 'donor-email.updated';
    }
}
