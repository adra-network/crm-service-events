<?php

namespace Adranetwork\CrmServiceEvents\DonorEmail;

use Adranetwork\CrmServiceEvents\Traits\EventHelper;

class DonorEmailCreatedEvent extends DonorEmailEvent
{
    use EventHelper;

    public function getEventName(): string
    {
        return 'donor-email.created';
    }
}
