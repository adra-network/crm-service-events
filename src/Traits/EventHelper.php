<?php

namespace Adranetwork\CrmServiceEvents\Traits;

use Adranetwork\CrmServiceEvents\Donation\DonationEvent;
use Adranetwork\CrmServiceEvents\Donor\DonorEvent;

use Adranetwork\CrmServiceEvents\DonorEmail\DonorEmailEvent;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\DonorEmail;

trait EventHelper
{
    public static function fromDonorModel(Donor $model): DonorEvent
    {
        return new self(
            null,
            $model->id,
            $model->first_name,
            $model->last_name,
            $model->gender->value ?? null,
            $model->birthday,
            $model->title,
            $model->salutation,
            $model->alternate_names ?? [],
            $model->created_at->toIso8601ZuluString(),
            $model->updated_at->toIso8601ZuluString()
        );
    }
    public static function fromDonationModel(Donation $model): DonationEvent
    {
        return new self(
            null,
            $model->id,
            $model->donor_id,
            $model->amount,
            $model->currency,
            $model->payment_provider_name,
            $model->payment_provider_reference_id,
            $model->status,
            $model->source,
            $model->info,
            $model->donated_at,
            $model->created_at,
            $model->updated_at
        );
    }
    public static function fromDonorEmailModel(DonorEmail $model): DonorEmailEvent
    {
        return new self(
            null,
            $model->id,
            $model->donor_id,
            $model->type->value ?? null,
            $model->status->value ?? null,
            $model->address,
            $model->default,
            $model->created_at->toIso8601ZuluString(),
            $model->updated_at->toIso8601ZuluString()
        );
    }
}
