<?php

namespace Adranetwork\CrmServiceEvents\Tests;

use Adranetwork\CrmServiceEvents\Donor\DonorCreatedEvent;
use App\Models\Donor;

class DonorEventTest extends TestCase
{
    /** @test **/
    public function it_can_be_created_via_model ()
    {
        $donor = Donor::factory()->create();
        $event = DonorCreatedEvent::fromDonorModel($donor);

        $this->assertInstanceOf(DonorCreatedEvent::class, $event);
    }
}
